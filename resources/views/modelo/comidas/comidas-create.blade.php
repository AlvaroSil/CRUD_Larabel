<!doctype html>
<html lang="es" dir="ltr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href='{{asset("css/normalize.css")}}' rel='stylesheet' type='text/css'>
    <link href='{{asset("css/styles.css")}}' rel='stylesheet' type='text/css'>
    <script src='{{asset("js/scripts.js")}}'></script>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.js" integrity="sha256-DrT5NfxfbHvMHux31Lkhxg42LY6of8TaYyK50jnxRnM=" crossorigin="anonymous"></script>
    <script src='{{asset("js/buscador.js")}}'></script>
    <title>PsykoShishas</title>
</head>
<body>
    <main>
        @include ('modelo.includesBack.navBack')
        <form method="POST" action="{{ route('comidas-store') }}" enctype="multipart/form-data">
            {{ csrf_field() }}
            <input type="text" name="nombre" class="form-control" placeholder="NOMBRE">
            <input type="text" name="ingredientes" class="form-control" placeholder="INGREDIENTES">
            <label for="imagen"><input class="insImg" type="file" name="imagen"  class="form-control">
            <button class="enviar" type="submit">ENVIAR</button>
            
        </form>
    </main>
</body>
</html>