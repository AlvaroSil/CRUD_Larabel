@extends('layouts.plantilla')

@section('content')
    @foreach ($comidas as $c)
        <a href="{{ route('comidas-show', $c->id) }}"><p>ID: {{ $c->id }}</p></a>
        <p>NOMBRE: {{ $c->nombre }}</p>
        <p>INGREDIENTES: {{ $c->ingredientes }}</p>
        <a href="{{ route('comidas-edit', $c->id) }}">Editar Comida</a>
        <a href="{{ route('comidas-destroy', $c->id) }}">Eliminar Comida</a>
        
        <br><br><br>
    @endforeach
@endsection
