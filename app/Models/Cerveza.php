<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cervezas extends Model
{
    use HasFactory;

    protected $table = 'cervezas';


    protected $fillable = [
        'origen','marca','imagen'
    ];
}
