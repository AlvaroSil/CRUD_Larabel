<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CoctelesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $coctel = DB::table('cocteles')->get();
        return view('modelo.cocteles.cocteles-index', compact('coctel'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('modelo.cocteles.cocteles-create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::table('cocteles')->insert(
            array(
                "nombre" => $request->input('nombre'),
                "descripcion" => $request->input('descripcion'),
                "imagen" => $request->file('imagen')->store('imgs','public'),
            )
        );
        return redirect()->action([PrivateController::class, 'listarCartaBack']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $coctel = Coctel::where('id', '=', $id)->first();
        return view('modelo.cocteles.cocteles-show', compact('coctel'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $coctel = DB::table('cocteles')->where('id', '=', $id)->first();
        return view('modelo.cocteles.cocteles-edit', compact('coctel'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $imagen=$request->file('imagen');
        if($imagen){
            DB::table('cocteles')->where('id', '=', $id)->update(
                array(
                    "nombre" => $request->input('nombre'),
                    "descripcion" => $request->input('descripcion'),
                    "imagen" => $request->file('imagen')->store('imgs','public'),
                )
            );
        }else{
            DB::table('cocteles')->where('id', '=', $id)->update(
                array(
                    "nombre" => $request->input('nombre'),
                    "descripcion" => $request->input('descripcion'),
                )
            );
        }
        
        return redirect()->action([PrivateController::class, 'listarRefrescosBack']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('cocteles')->where('id', '=', $id)->delete();
        return redirect()->back();
    }
}
