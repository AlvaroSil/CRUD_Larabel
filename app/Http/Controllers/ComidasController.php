<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Comida;
use DB;

class ComidasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $comida = DB::table('comidas')->get();
        return view('modelo.comidas.comidas-index', compact('comida'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('modelo.comidas.comidas-create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::table('comidas')->insert(
            array(
                "nombre"=>$request->input('nombre'),
                "ingredientes"=>$request->input('ingredientes'),
                "imagen" => $request->file('imagen')->store('imgs','public'),
            )
        );
        return redirect()->action([PrivateController::class, 'listarCartaBack']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $comida = Coctel::where('id', '=', $id)->first();
        return view('modelo.comidas.comidas-show', compact('comida'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $comida = DB::table('comidas')->where('id', '=', $id)->first();
        return view('modelo.comidas.comidas-edit', compact('comida'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $imagen=$request->file('imagen');
        if($imagen){
            DB::table('comidas')->where('id', '=', $id)->update(
                array(
                    "nombre" => $request->input('nombre'),
                    "ingredientes" => $request->input('ingredientes'),
                    "imagen" => $request->file('imagen')->store('imgs','public'),
                )
            );
        }else{
            DB::table('comidas')->where('id', '=', $id)->update(
                array(
                    "nombre" => $request->input('nombre'),
                    "ingredientes" => $request->input('ingredientes'),
                )
            );
        }
        
        return redirect()->action([PrivateController::class, 'listarRefrescosBack']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('comidas')->where('id', '=', $id)->delete();
        return redirect()->back();
    }
}
