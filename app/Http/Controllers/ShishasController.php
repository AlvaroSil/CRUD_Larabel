<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Shisha;
use DB;
use Storage;


class ShishasController extends Controller{
   
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {        
        $shisha = DB::table('shishas')->get();
        return view('modelo.shishas.shishas-index', compact('shisha'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('modelo.shishas.shishas-create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request['imagen']);
       // $imagen = $request->file('imagen');
       
        DB::table('shishas')->insert(
            array(
                "marca" => $request->input('marca'),
                "modelo" => $request->input('modelo'),
                "imagen" => $request->file('imagen')->store('imgs','public'),
                )
            );
        // Storage::putFileAs("public/imgs", $request['imagen'],$request['imagen']->getClientOriginalName());
        return redirect()->action([PrivateController::class, 'listarShishasBack']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $shisha = Shisha::where('id', '=', $id)->first();
        return view('modelo.shishas.shishas-show', compact('shisha'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $shisha = DB::table('shishas')->where('id', '=', $id)->first();
        return view('modelo.shishas.shishas-edit', compact('shisha'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $imagen=$request->file('imagen');
        if($imagen){
            DB::table('shishas')->where('id', '=', $id)->update(
                array(
                    "marca" => $request->input('marca'),
                    "modelo" => $request->input('modelo'),
                    "imagen" => $request->file('imagen')->store('imgs','public'),
                )
            );
        }else{
            DB::table('shishas')->where('id', '=', $id)->update(
                array(
                    "marca" => $request->input('marca'),
                    "modelo" => $request->input('modelo'),
                )
            );
        }

        return redirect()->action([PrivateController::class, 'listarShishasBack']);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('shishas')->where('id', '=', $id)->delete();
        return redirect()->back();
    }
}
