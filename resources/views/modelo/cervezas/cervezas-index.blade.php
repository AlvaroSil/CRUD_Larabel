@extends('layouts.plantilla')


@section('content')
    @foreach ($cerveza as $c)
        <a href="{{ route('cervezas-show', $c->id) }}"><p>ID: {{ $c->id }}</p></a>
        <p>MARCA: {{ $c->marca }}</p>
        <p>MODELO: {{ $c->modelo }}</p>
        <a href="{{ route('cervezas-edit', $c->id) }}">Editar Cerveza</a>
        <a href="{{ route('cervezas-destroy', $c->id) }}">Eliminar Cerveza</a>
        
        <br><br><br>
    @endforeach
@endsection
