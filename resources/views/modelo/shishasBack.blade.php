<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href='{{asset("css/normalize.css")}}' rel='stylesheet' type='text/css'>
    <link href='{{asset("css/styles.css")}}' rel='stylesheet' type='text/css'>
    <script src='{{asset("js/scripts.js")}}'></script>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.js" integrity="sha256-DrT5NfxfbHvMHux31Lkhxg42LY6of8TaYyK50jnxRnM=" crossorigin="anonymous"></script>
    <script src='{{asset("js/buscador.js")}}'></script>
    <title>PsykoShishas</title>
</head>
<body>
<main>
@include ('modelo.includesBack.navBack')

    <div class="cabecera">
        <img class="banner" src='{{asset("img/shisha_angel-01.png")}}'>
        <h1>Saborea la experiencia</h1>
    </div>
    <center><a href="{{route('shishas-create')}}"><button class="añadir">Añadir Shisha</button></a></center>
    <br>
    <div id="shisha" class="titulo">
    <center><input id="buscador" type="text"></center>
    <br>
    @foreach ($shisha as $s)    
    <div class="producto">
        <a href="{{ route('shishas-show', $s->id) }}"></a>
        <img src='{{asset("storage/$s->imagen")}}'>  
        <h2>{{ $s->marca }} {{ $s->modelo }}</h2>
        <a href="{{ route('shishas-edit', $s->id) }}"><button class="editar">Editar Shisha</button></a>
        <a href="{{ route('shishas-destroy', $s->id) }}"><button class="eliminar">Eliminar Shisha</button></a>
     
    </div>

   
    @endforeach
        
    </div>
    <pre>








































































    </pre>
    @include ('modelo.includesBack.footerBack')


</main>

</body>
</html>