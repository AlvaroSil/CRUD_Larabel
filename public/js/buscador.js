$(document).ready(function(){
    $("#buscador").on("keyup", function() {
      var value = $(this).val().toLowerCase();
      $(".producto").filter(function() {
        $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
      });
    });
  });