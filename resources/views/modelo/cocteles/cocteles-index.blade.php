@extends('layouts.plantilla')

@section('content')
    @foreach ($cocteles as $b)
        <a href="{{ route('cocteles-show', $b->id) }}"><p>ID: {{ $b->id }}</p></a>
        <p>NOMBRE: {{ $b->nombre }}</p>
        <p>DESCRIPCION: {{ $b->descripcion }}</p>
        <a href="{{ route('cocteles-edit', $b->id) }}">Editar Coctel</a>
        <a href="{{ route('cocteles-destroy', $b->id) }}">Eliminar Coctel</a>
        
        <br><br><br>
    @endforeach
@endsection