<!-- Font Awesome -->
<link rel="stylesheet" href="{{ asset('lte/plugins/fontawesome-free/css/all.min.css' )}}">
<!-- Ionicons -->
<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css' )}}">
<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('lte/plugins/datatables-bs4/css/dataTables.bootstrap4.css' )}}">
<!-- Theme style -->
<link rel="stylesheet" href="{{ asset('lte/dist/css/adminlte.min.css' )}}">
<!-- Google Font: Source Sans Pro -->
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

<!-- Bootstrap 4 -->
<script src="{{ asset('lte/plugins/bootstrap/js/bootstrap.bundle.min.js' )}}"></script>
<!-- DataTables -->
<script src="{{ asset('lte/plugins/datatables/jquery.dataTables.js' )}}"></script>
<script src="{{ asset('lte/plugins/datatables-bs4/js/dataTables.bootstrap4.js' )}}"></script>

<script>
  $(function () {
    $('#tablaEntrenadores').DataTable({
      'paging'      : true,
      'lengthChange': true,
      'searching'   : true,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : true,
      'language': {
            "lengthMenu": "Mostrar _MENU_ entradas por página",
            "zeroRecords": "Ninguna entrada disponible",
            "info": "Mostrar página _PAGE_ de _PAGES_",
            "infoEmpty": "No records available",
            "infoFiltered": "(filtrar por _MAX_ entradas)",
            "paginate": {
              "first":      "First",
              "last":       "Last",
              "next":       "Siguiente",
              "previous":   "Anterior"
          },
          "search": "Filtrar:",
        }
    });
    $('#tablaEquipos').DataTable({
      'paging'      : true,
      'lengthChange': true,
      'searching'   : true,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : true,
      'language': {
            "lengthMenu": "Mostrar _MENU_ entradas por página",
            "zeroRecords": "Ninguna entrada disponible",
            "info": "Mostrar página _PAGE_ de _PAGES_",
            "infoEmpty": "No records available",
            "infoFiltered": "(filtrar por _MAX_ entradas)",
            "paginate": {
              "first":      "First",
              "last":       "Last",
              "next":       "Siguiente",
              "previous":   "Anterior"
          },
          "search": "Filtrar:",
        }
    });
    $('#tablaClubs').DataTable({
      'paging'      : true,
      'lengthChange': true,
      'searching'   : true,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : true,
      'language': {
            "lengthMenu": "Mostrar _MENU_ entradas por página",
            "zeroRecords": "Ninguna entrada disponible",
            "info": "Mostrar página _PAGE_ de _PAGES_",
            "infoEmpty": "No records available",
            "infoFiltered": "(filtrar por _MAX_ entradas)",
            "paginate": {
              "first":      "First",
              "last":       "Last",
              "next":       "Siguiente",
              "previous":   "Anterior"
          },
          "search": "Filtrar:",
        }
    });
    $('#tablaUsuarios').DataTable({
      'paging'      : true,
      'lengthChange': true,
      'searching'   : true,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : true,
      'language': {
            "lengthMenu": "Mostrar _MENU_ entradas por página",
            "zeroRecords": "Ninguna entrada disponible",
            "info": "Mostrar página _PAGE_ de _PAGES_",
            "infoEmpty": "No records available",
            "infoFiltered": "(filtrar por _MAX_ entradas)",
            "paginate": {
              "first":      "First",
              "last":       "Last",
              "next":       "Siguiente",
              "previous":   "Anterior"
          },
          "search": "Filtrar:",
        }
    });
    $('#tablaJugadores').DataTable({
      'paging'      : true,
      'lengthChange': true,
      'searching'   : true,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : true,
      'language': {
            "lengthMenu": "Mostrar _MENU_ entradas por página",
            "zeroRecords": "Ninguna entrada disponible",
            "info": "Mostrar página _PAGE_ de _PAGES_",
            "infoEmpty": "No records available",
            "infoFiltered": "(filtrar por _MAX_ entradas)",
            "paginate": {
              "first":      "First",
              "last":       "Last",
              "next":       "Siguiente",
              "previous":   "Anterior"
          },
          "search": "Filtrar:",
        }
    })
  });
  $('#tablaDocumentos').DataTable({
      'paging'      : true,
      'lengthChange': true,
      'searching'   : true,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : true,
      'language': {
            "lengthMenu": "Mostrar _MENU_ entradas por página",
            "zeroRecords": "Ninguna entrada disponible",
            "info": "Mostrar página _PAGE_ de _PAGES_",
            "infoEmpty": "No records available",
            "infoFiltered": "(filtrar por _MAX_ entradas)",
            "paginate": {
              "first":      "First",
              "last":       "Last",
              "next":       "Siguiente",
              "previous":   "Anterior"
          },
          "search": "Filtrar:",
        }
    });
    $('#tablaDomiciliaciones').DataTable({
      'paging'      : true,
      'lengthChange': true,
      'searching'   : true,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : true,
      'language': {
            "lengthMenu": "Mostrar _MENU_ entradas por página",
            "zeroRecords": "Ninguna entrada disponible",
            "info": "Mostrar página _PAGE_ de _PAGES_",
            "infoEmpty": "No records available",
            "infoFiltered": "(filtrar por _MAX_ entradas)",
            "paginate": {
              "first":      "First",
              "last":       "Last",
              "next":       "Siguiente",
              "previous":   "Anterior"
          },
          "search": "Filtrar:",
        }
    });
    $('#tablaAccesos').DataTable({
      'paging'      : true,
      'lengthChange': true,
      'searching'   : true,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : true,
      'language': {
            "lengthMenu": "Mostrar _MENU_ entradas por página",
            "zeroRecords": "Ninguna entrada disponible",
            "info": "Mostrar página _PAGE_ de _PAGES_",
            "infoEmpty": "No records available",
            "infoFiltered": "(filtrar por _MAX_ entradas)",
            "paginate": {
              "first":      "First",
              "last":       "Last",
              "next":       "Siguiente",
              "previous":   "Anterior"
          },
          "search": "Filtrar:",
        }
    });

</script>
