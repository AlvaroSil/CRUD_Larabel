<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Refresco;
use DB;
use Storage;

class RefrescosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $refresco = DB::table('refrescos')->get();
        return view('modelo.refrescos.refrescos-index', compact('refresco'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('modelo.refrescos.refrescos-create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::table('refrescos')->insert(
            array(
                "nombre" => $request->input('nombre'),
                "imagen" => $request->file('imagen')->store('imgs','public'),
            )
        );
        return redirect()->action([PrivateController::class, 'listarRefrescosBack']);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $refresco = Refreco::where('id', '=', $id)->first();
        return view('modelo.refrescos.refrescos-show', compact('refresco'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $refresco = DB::table('refrescos')->where('id', '=', $id)->first();
        return view('modelo.refrescos.refrescos-edit', compact('refresco'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $imagen=$request->file('imagen');
        if($imagen){
            DB::table('refrescos')->where('id', '=', $id)->update(
                array(
                    "nombre" => $request->input('nombre'),
                    "imagen" => $request->file('imagen')->store('imgs','public'),
                )
            );
        }else{
            DB::table('refrescos')->where('id', '=', $id)->update(
                array(
                    "nombre" => $request->input('nombre'),                )
            );
        }
        
        return redirect()->action([PrivateController::class, 'listarRefrescosBack']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('refrescos')->where('id', '=', $id)->delete();
        return redirect()->back();
    }
}
