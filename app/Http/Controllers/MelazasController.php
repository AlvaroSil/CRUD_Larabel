<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Melaza;
use DB;

class MelazasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */ 
    public function index()
    {
        $melaza = DB::table('melazas')->get();
        return view('modelo.melazas.melazas-index', compact('melaza'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('modelo.melazas.melazas-create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::table('melazas')->insert(
            array(
                "marca"=>$request->input('marca'),
                "sabor"=>$request->input('sabor'),
                "imagen" => $request->file('imagen')->store('imgs','public'),

            )
        );
        return redirect()->action([PrivateController::class, 'listarCartaBack']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $melaza = Melaza::where('id', '=', $id)->first();
        return view('modelo.melazas.melazas-show', compact('melaza'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $melaza = Melaza::where('id', '=', $id)->first();
        return view('modelo.melazas.melazas-edit', compact('melazas'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $imagen=$request->file('imagen');
        if($imagen){
            DB::table('melazas')->where('id', '=', $id)->update(
                array(
                    "marca" => $request->input('marca'),
                    "sabor" => $request->input('sabor'),
                    "imagen" => $request->file('imagen')->store('imgs','public'),
                )
            );
        }else{
            DB::table('melazas')->where('id', '=', $id)->update(
                array(
                    "marca" => $request->input('marca'),
                    "sabor" => $request->input('sabor'),
                )
            );
        }
        
        return redirect()->action([PrivateController::class, 'listarRefrescosBack']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Melaza::where('id', '=', $id)->delete();
        return redirect()->back();
    }
}
