<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Batido;
use DB;
use Storage;


class BatidosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $batido = DB::table('batidos')->get();
        return view('modelo.batidos.batidos-index', compact('batido'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('modelo.batidos.batidos-create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::table('batidos')->insert(
            array(
                "nombre" => $request->input('nombre'),
                "descripcion" => $request->input('descripcion'),
                "imagen" => $request->file('imagen')->store('imgs','public'),
            )
        );
        return redirect()->action([PrivateController::class, 'listarCartaBack']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $batido = Batido::where('id', '=', $id)->first();
        return view('modelo.batidos.batidos-show', compact('batido'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $batido = DB::table('batidos')->where('id', '=', $id)->first();
        return view('modelo.batidos.batidos-edit', compact('batido'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $imagen=$request->file('imagen');
        if($imagen){
            DB::table('batidos')->where('id', '=', $id)->update(
                array(
                    "nombre" => $request->input('nombre'),
                    "descripcion" => $request->input('descripcion'),
                    "imagen" => $request->file('imagen')->store('imgs','public'),
                )
            );
        }else{
            DB::table('batidos')->where('id', '=', $id)->update(
                array(
                    "nombre" => $request->input('nombre'),
                    "descripcion" => $request->input('descripcion'),
                )
            );
        }
        
        return redirect()->action([PrivateController::class, 'listarRefrescosBack']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('batidos')->where('id', '=', $id)->delete();
        return redirect()->back();
    }
}
