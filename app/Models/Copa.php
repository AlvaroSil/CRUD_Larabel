<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Copas extends Model
{
    use HasFactory;

    protected $table = 'copas';


    protected $fillable = [
        'tipo','marca','imagen'
    ];
}
