<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Cerveza;
use DB;

class CervezasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cerveza = DB::table('cervezas')->get();
        return view('modelo.cervezas.cervezas-index', compact('cerveza'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('modelo.cervezas.cervezas-create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::table('cervezas')->insert(
            array(
                "origen" => $request->input('origen'),
                "marca" => $request->input('marca'),
                "imagen" => $request->file('imagen')->store('imgs','public'),
            )
        );
        return redirect()->action([PrivateController::class, 'listarCartaBack']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $cerveza = Cerveza::where('id', '=', $id)->first();
        return view('modelo.cervezas.cervezas-show', compact('cerveza'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cerveza = DB::table('cervezas')->where('id', '=', $id)->first();
        return view('modelo.cervezas.cervezas-edit', compact('cerveza'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $imagen=$request->file('imagen');
        if($imagen){
            DB::table('cervezas')->where('id', '=', $id)->update(
                array(
                    "origen" => $request->input('origen'),
                    "marca" => $request->input('marca'),
                    "imagen" => $request->file('imagen')->store('imgs','public'),
                )
            );
        }else{
            DB::table('cervezas')->where('id', '=', $id)->update(
                array(
                    "origen" => $request->input('origen'),
                    "marca" => $request->input('marca'),
                )
            );
        }
        
        return redirect()->action([PrivateController::class, 'listarRefrescosBack']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('cervezas')->where('id', '=', $id)->delete();
        return redirect()->back();
    }
}
