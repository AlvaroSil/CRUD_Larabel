<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cocteles extends Model
{
    use HasFactory;

    protected $table = 'cocteles';


    protected $fillable = [
        'nombre','descripcion','imagen'
    ];
}
