<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href='{{asset("css/normalize.css")}}' rel='stylesheet' type='text/css'>
    <link href='{{asset("css/styles.css")}}' rel='stylesheet' type='text/css'>
    <script src='{{asset("js/scripts.js")}}'></script>
    <title>PsykoShishas</title>
</head>
<body>
<main>
@include ('includes.nav')

    <h1>!Te esperamos¡</h1>
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d759.9679307252596!2d-3.5976946707696658!3d40.3673685987107!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd4224dc8f9dfbab%3A0x692d8f283852bc27!2sCalle%20Adolfo%20Bioy%20Casares%2C%207%2C%2028031%20Madrid!5e0!3m2!1ses!2ses!4v1607821260049!5m2!1ses!2ses"
            width="100%" height="400" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false"
            tabindex="0">
    </iframe>
    <h2><img class="insta" src="img/instagram.svg"> <a href="http://www.instagram.com/psykoshishas">PSYKOSHISHAS
    </h2></a>
    <script src="https://snapwidget.com/js/snapwidget.js"></script>
    <iframe src="https://snapwidget.com/embed/898100" class="snapwidget-widget" allowtransparency="true" frameborder="0"
            scrolling="no" style="border:none; overflow:hidden; width: 50%"></iframe>
    <iframe src="https://open.spotify.com/embed/playlist/7LK5CE1tJ5AD6ysXmgI55z" width="50%" height="380"
            frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>

            @include ('includes.footer')

</main>

</body>
</html>