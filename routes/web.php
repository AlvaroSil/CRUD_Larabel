<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ShishasController;
use App\Http\Controllers\RefrescosController;
use App\Http\Controllers\ComidasController;
use App\Http\Controllers\MelazasController;
use App\Http\Controllers\CervezasController;
use App\Http\Controllers\CopasController;
use App\Http\Controllers\BatidosController;
use App\Http\Controllers\CoctelesController;
use App\Http\Controllers\PublicController;
use App\Http\Controllers\PrivateController;
use App\Models\Shisha;
use App\Models\Refresco;
use App\Models\Comida;
use App\Models\Melaza;
use App\Models\Cerveza;
use App\Models\Copa;
use App\Models\Batido;
use App\Models\Coctel;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

/*Vistas Privadas*/
Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

Route::get('/modelo', function () {
    return view('modelo/indexBack');
})->middleware(['auth'])->name('indexBack');

Route::get('/modelo/shishas', [PrivateController::class, 'listarShishasBack'] )->middleware(['auth'])->name('shishaBack');
Route::get('/modelo/carta',[PrivateController::class, 'listarRefrescosBack'])->middleware(['auth'])->name('cartaBack');


require __DIR__.'/auth.php';

//Route::get('/', function () {
  //  return view('index');
//});

/*Vistas publicas*/

Route::view('/index', 'index')->name('index');
Route::view('/aboutUs', 'aboutUs')->name('aboutUs');
Route::view('/contacto', 'contacto')->name('contacto');
Route::get('/shishas',[PublicController::class, 'listarShishas'])->name('shisha');
Route::get('/carta',[PublicController::class, 'listarRefrescos'])->name('carta');


/*Vistas Privadas*/
//Route::view('/modelo', 'modelo/indexBack')->name('indexBack');
//Route::view('/modelo/shishasNav', 'modelo/shishas/shishas-index')->name('shishasBack');
//Route::view('/modelo/carta', 'modelo/cartaBack')->name('cartaBack');
// Route::get('/modelo/shishas',[PrivateController::class, 'listarShishasBack'])->name('shishaBack');
// Route::get('/modelo/carta',[PrivateController::class, 'listarRefrescosBack'])->name('cartaBack');

/*Conexiones Shishas*/
//Route::get('/modelo/shishas',[ShishasController::class, 'index'])->name('shishas-index');
Route::get('/modelo/shishas/create', [ShishasController::class, 'create'])->middleware(['auth'])->name('shishas-create');
Route::post('/modelo/shishas/store', [ShishasController::class, 'store'])->middleware(['auth'])->name('shishas-store');
Route::get('/modelo/shishas/show/{id}', [ShishasController::class, 'show'])->middleware(['auth'])->name('shishas-show');
Route::get('/modelo/shishas/edit/{id}', [ShishasController::class, 'edit'])->middleware(['auth'])->name('shishas-edit');
Route::post('/modelo/shishas/update/{id}', [ShishasController::class, 'update'])->middleware(['auth'])->name('shishas-update');
Route::any('/modelo/shishas/destroy/{id}', [ShishasController::class, 'destroy'])->middleware(['auth'])->name('shishas-destroy');

/*Conexiones Comidas*/  
Route::get('/modelo/comidas',[ComidasController::class, 'index'])->middleware(['auth'])->name('comidas-index');
Route::get('/modelo/comidas/create', [ComidasController::class, 'create'])->middleware(['auth'])->name('comidas-create');
Route::post('/modelo/comidas/store', [ComidasController::class, 'store'])->middleware(['auth'])->name('comidas-store');
Route::get('/modelo/comidas/show/{id}',  [ComidasController::class, 'show'])->middleware(['auth'])->name('comidas-show');
Route::get('/modelo/comidas/edit/{id}', [ComidasController::class, 'edit'])->middleware(['auth'])->name('comidas-edit');
Route::post('/modelo/comidas/update/{id}', [ComidasController::class, 'update'])->middleware(['auth'])->name('comidas-update');
Route::any('/modelo/comidas/destroy/{id}', [ComidasController::class, 'destroy'])->middleware(['auth'])->name('comidas-destroy');

/*Conexiones Refrescos*/
Route::get('/modelo/refrescos',[RefrescosController::class, 'index'])->middleware(['auth'])->name('refrescos-index');
Route::get('/modelo/refrescos/create', [RefrescosController::class, 'create'])->middleware(['auth'])->name('refrescos-create');
Route::post('/modelo/refrescos/store', [RefrescosController::class, 'store'])->middleware(['auth'])->name('refrescos-store');
Route::get('/modelo/refrescos/show/{id}', [RefrescosController::class, 'show'])->middleware(['auth'])->name('refrescos-show');
Route::get('/modelo/refrescos/edit/{id}', [RefrescosController::class, 'edit'])->middleware(['auth'])->name('refrescos-edit');
Route::post('/modelo/refrescos/update/{id}', [RefrescosController::class, 'update'])->middleware(['auth'])->name('refrescos-update');
Route::any('/modelo/refrescos/destroy/{id}', [RefrescosController::class, 'destroy'])->middleware(['auth'])->name('refrescos-destroy');

/*Conexiones Melazas*/
Route::get('/modelo/melazas',[MelazasController::class, 'index'])->middleware(['auth'])->name('melazas-index');
Route::get('/modelo/melazas/create', [MelazasController::class, 'create'])->middleware(['auth'])->name('melazas-create');
Route::post('/modelo/melazas/store', [MelazasController::class, 'store'])->middleware(['auth'])->name('melazas-store');
Route::get('/modelo/melazas/show/{id}', [MelazasController::class, 'show'])->middleware(['auth'])->name('melazas-show');
Route::get('/modelo/melazas/edit/{id}', [MelazasController::class, 'edit'])->middleware(['auth'])->name('melazas-edit');
Route::post('/modelo/melazas/update/{id}', [MelazasController::class, 'update'])->middleware(['auth'])->name('melazas-update');
Route::any('/modelo/melazas/destroy/{id}', [MelazasController::class, 'destroy'])->middleware(['auth'])->name('melazas-destroy');

/*Conexiones Batidos*/
Route::get('/modelo/batidos',[BatidosController::class, 'index'])->middleware(['auth'])->name('batidos-index');
Route::get('/modelo/batidos/create', [BatidosController::class, 'create'])->middleware(['auth'])->name('batidos-create');
Route::post('/modelo/batidos/store', [BatidosController::class, 'store'])->middleware(['auth'])->name('batidos-store');
Route::get('/modelo/batidos/show/{id}', [BatidosController::class, 'show'])->middleware(['auth'])->name('batidos-show');
Route::get('/modelo/batidos/edit/{id}', [BatidosController::class, 'edit'])->middleware(['auth'])->name('batidos-edit');
Route::post('/modelo/batidos/update/{id}', [BatidosController::class, 'update'])->middleware(['auth'])->name('batidos-update');
Route::any('/modelo/batidos/destroy/{id}', [BatidosController::class, 'destroy'])->middleware(['auth'])->name('batidos-destroy');

/*Conexiones Cervezas*/
Route::get('/modelo/cervezas',[CervezasController::class, 'index'])->middleware(['auth'])->name('cervezas-index');
Route::get('/modelo/cervezas/create', [CervezasController::class, 'create'])->middleware(['auth'])->name('cervezas-create');
Route::post('/modelo/cervezas/store', [CervezasController::class, 'store'])->middleware(['auth'])->name('cervezas-store');
Route::get('/modelo/cervezas/show/{id}', [CervezasController::class, 'show'])->middleware(['auth'])->name('cervezas-show');
Route::get('/modelo/cervezas/edit/{id}', [CervezasController::class, 'edit'])->middleware(['auth'])->name('cervezas-edit');
Route::post('/modelo/cervezas/update/{id}', [CervezasController::class, 'update'])->middleware(['auth'])->name('cervezas-update');
Route::any('/modelo/cervezas/destroy/{id}', [CervezasController::class, 'destroy'])->middleware(['auth'])->name('cervezas-destroy');

/*Conexiones Copas*/
Route::get('/modelo/copas',[CopasController::class, 'index'])->middleware(['auth'])->name('copas-index');
Route::get('/modelo/copas/create', [CopasController::class, 'create'])->middleware(['auth'])->name('copas-create');
Route::post('/modelo/copas/store', [CopasController::class, 'store'])->middleware(['auth'])->name('copas-store');
Route::get('/modelo/copas/show/{id}', [CopasController::class, 'show'])->middleware(['auth'])->name('copas-show');
Route::get('/modelo/copas/edit/{id}', [CopasController::class, 'edit'])->middleware(['auth'])->name('copas-edit');
Route::post('/modelo/copas/update/{id}', [CopasController::class, 'update'])->middleware(['auth'])->name('copas-update');
Route::any('/modelo/copas/destroy/{id}', [CopasController::class, 'destroy'])->middleware(['auth'])->name('copas-destroy');

/*Conexiones Cocteles*/
Route::get('/modelo/cocteles',[CoctelesController::class, 'index'])->middleware(['auth'])->name('cocteles-index');
Route::get('/modelo/cocteles/create', [CoctelesController::class, 'create'])->middleware(['auth'])->name('cocteles-create');
Route::post('/modelo/cocteles/store', [CoctelesController::class, 'store'])->middleware(['auth'])->name('cocteles-store');
Route::get('/modelo/cocteles/show/{id}', [CoctelesController::class, 'show'])->middleware(['auth'])->name('cocteles-show');
Route::get('/modelo/cocteles/edit/{id}', [CoctelesController::class, 'edit'])->middleware(['auth'])->name('cocteles-edit');
Route::post('/modelo/cocteles/update/{id}', [CoctelesController::class, 'update'])->middleware(['auth'])->name('cocteles-update');
Route::any('/modelo/cocteles/destroy/{id}', [CoctelesController::class, 'destroy'])->middleware(['auth'])->name('cocteles-destroy');

