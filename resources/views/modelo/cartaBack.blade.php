<!doctype html>
<html lang="es" dir="ltr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href='{{asset("css/normalize.css")}}' rel='stylesheet' type='text/css'>
    <link href='{{asset("css/styles.css")}}' rel='stylesheet' type='text/css'>
    <script src='{{asset("js/scripts.js")}}'></script>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.js" integrity="sha256-DrT5NfxfbHvMHux31Lkhxg42LY6of8TaYyK50jnxRnM=" crossorigin="anonymous"></script>
    <script src='{{asset("js/buscador.js")}}'></script>
    <title>PsykoShishas</title>
</head>

<main>
    
    @include ('modelo.includesBack.navBack')

    <a href="{{route('refrescos-create')}}">Añadir Refresco</a>
    <br>
    <a href="{{route('batidos-create')}}">Añadir Batido</a>
    <br>
    <a href="{{route('cervezas-create')}}">Añadir Cerveza</a>
    <br>
    <a href="{{route('copas-create')}}">Añadir Copa</a>
    <br>
    <a href="{{route('cocteles-create')}}">Añadir Coctel</a>
    <br>
    <a href="{{route('comidas-create')}}">Añadir Comida</a>
    <br>
    <a href="{{route('melazas-create')}}">Añadir Melaza</a>

    <div class="cabecera">
        <img class="banner" src='{{asset("img/carta_-01.png")}}'>
        <h1>Descubre nuestra carta</h1>
    </div>
    <center><input id="buscador" type="text"></center>
    <div class="lista">
        <ul>
            <li><a href="#refrescos">Refrescos</a></li>
            <li><a href="#batidos">Batidos</a></li>
            <li><a href="#cervezas">Cervezas</a></li>
            <li><a href="#copas">Copas</a></li>
            <li><a href="#cocteles">Cocteles</a></li>
            <li><a href="#comida">Comida</a></li>
            <li><a href="#melazas">Melazas</a></li>
        </ul>
    </div>
    <br>
    <div id="refrescos" class="titulo">
        <h2>Refrescos</h2>
        @foreach ($refresco as $r)
        <div class="producto">
            <img src='{{asset("storage/$r->imagen")}}'>             
            <a href="{{ route('refrescos-show', $r->id) }}"></a>
            <h2>{{ $r->nombre }}</h2>
            <a href="{{ route('refrescos-edit', $r->id) }}">Editar Refresco</a>
            <a href="{{ route('refrescos-destroy', $r->id) }}">Eliminar Refresco</a>
        </div>
        @endforeach
    </div>
    <br>
    <div id="batidos" class="titulo">
        <h2>Batidos</h2>
        @foreach ($batido as $b)
        <div class="producto">
            <img src='{{asset("storage/$b->imagen")}}'>             
            <a href="{{ route('batidos-show', $b->id) }}"></a>
            <h2>{{ $b->nombre }}</h2>
            <a href="{{ route('batidos-edit', $b->id) }}">Editar Batido</a>
            <a href="{{ route('batidos-destroy', $b->id) }}">Eliminar Batido</a>
        </div>
        @endforeach
    </div>
    <div id="cervezas" class="titulo">
        <h2>Cervezas</h2>
        @foreach ($cerveza as $c)
        <div class="producto">
            <img src='{{asset("storage/$c->imagen")}}'>             
            <a href="{{ route('cervezas-show', $c->id) }}"></a>
            <h2>{{ $c->marca }}</h2>
            <a href="{{ route('cervezas-edit', $c->id) }}">Editar Cerveza</a>
            <a href="{{ route('cervezas-destroy', $c->id) }}">Eliminar Cerveza</a>

        </div>
        @endforeach
    </div>
    <div id="copas" class="titulo">
        <h2>Copas</h2>
        @foreach ($copa as $s)
        <div class="producto">
            <img src='{{asset("storage/$s->imagen")}}'>             
            <a href="{{ route('copas-show', $b->id) }}"></a>
            <h2>{{ $s->tipo }} {{ $s->marca }}</h2>
            <a href="{{ route('copas-edit', $s->id) }}">Editar Copa</a>
            <a href="{{ route('copas-destroy', $s->id) }}">Eliminar Copa</a>
        </div>
        @endforeach
    </div>
    <div id="cocteles" class="titulo">
        <h2>Cocteles</h2>
        @foreach ($coctel as $b)
        <div class="producto">
            <img src='{{asset("storage/$b->imagen")}}'>             
            <a href="{{ route('cocteles-show', $b->id) }}"></a>
            <h2>{{ $b->nombre }}</h2>
            <a href="{{ route('cocteles-edit', $b->id) }}">Editar Coctel</a>
            <a href="{{ route('cocteles-destroy', $b->id) }}">Eliminar Coctel</a>
        </div>
        @endforeach
    </div>
    <div id="comida" class="titulo">
        <h2>Comida</h2>
        @foreach ($comida as $c)
        <div class="producto">
            <img src='{{asset("storage/$c->imagen")}}'>             
            <a href="{{ route('comidas-show', $c->id) }}"></a>
            <h2>{{ $c->nombre }}</h2>
            <a href="{{ route('comidas-edit', $c->id) }}">Editar Comida</a>
            <a href="{{ route('comidas-destroy', $c->id) }}">Eliminar Comida</a>
        </div>
        @endforeach
    </div>
    <div id="melazas" class="titulo">
        <h2>Melazas</h2>
        @foreach ($melaza as $m)
        <div class="producto">
            <img src='{{asset("storage/$m->imagen")}}'>             
            <a href="{{ route('melazas-show', $m->id) }}"></a>
            <h2>{{ $m->marca }}</h2>
            <a href="{{ route('melazas-edit', $m->id) }}">Editar Melaza</a>
            <a href="{{ route('melazas-destroy', $m->id) }}">Eliminar Melaza</a>
        </div>
        @endforeach
    </div>

    
<pre>

</pre>

     
    @include ('modelo.includesBack.footerBack')
    


</main>
</body>
</html>