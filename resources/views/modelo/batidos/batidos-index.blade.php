@extends('layouts.plantilla')

@section('content')
    @foreach ($batido as $b)
        <a href="{{ route('batidos-show', $b->id) }}"><p>ID: {{ $b->id }}</p></a>
        <p>NOMBRE: {{ $b->nombre }}</p>
        <p>DESCRIPCION: {{ $b->descripcion }}</p>
        <a href="{{ route('batidos-edit', $b->id) }}">Editar Batido</a>
        <a href="{{ route('batidos-destroy', $b->id) }}">Eliminar Batido</a>
        
        <br><br><br>
    @endforeach
@endsection
