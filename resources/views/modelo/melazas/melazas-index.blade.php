@extends('layouts.plantilla')

@section('content')
    @foreach ($melaza as $m)
        <a href="{{ route('melazas-show', $m->id) }}"><p>ID: {{ $m->id }}</p></a>
        <p>MARCA: {{ $m->marca }}</p>
        <p>SABOR: {{ $m->sabor }}</p>
        <a href="{{ route('melazas-edit', $m->id) }}">Editar Melaza</a>
        <a href="{{ route('melazas-destroy', $m->id) }}">Eliminar Melaza</a>
        
        <br><br><br>
    @endforeach
@endsection
