<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;


class PrivateController extends Controller
{
    public function listarShishasBack()
    {        
        $shisha = DB::table('shishas')->get();
        return view('modelo/shishasBack', compact('shisha'));
    }
    public function listarRefrescosBack()
    {        
        $refresco = DB::table('refrescos')->get();
        $batido = DB::table('batidos')->get();
        $cerveza = DB::table('cervezas')->get();
        $copa = DB::table('copas')->get();
        $coctel = DB::table('cocteles')->get();
        $comida = DB::table('comidas')->get();
        $melaza = DB::table('melazas')->get();
        return view('modelo/cartaBack', compact('batido','refresco','cerveza','copa','coctel','comida','melaza'));
    }
}
