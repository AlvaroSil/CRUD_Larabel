<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Schema;


class Refrescos extends Model
{
    use HasFactory;

    protected $table = 'refrescos';

    protected $fillable = [
        'nombre','imagen'
    ];
}
