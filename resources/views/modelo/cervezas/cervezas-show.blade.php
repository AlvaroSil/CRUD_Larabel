<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Shishas Show</title>
</head>
<body>
    <form method="get">
        {{ csrf_field() }}
        <input type="text" name="origen" value="{{ $cerveza->origen }}" placeholder="ORIGEN">
        <input type="text" name="marca" value="{{ $cerveza->marca }}" placeholder="MARCA">
     </form>
</body>
</html>
