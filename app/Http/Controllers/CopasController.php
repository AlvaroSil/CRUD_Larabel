<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Copa;
use DB;

class CopasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $copa = DB::table('copas')->get();
        return view('modelo.copas.copas-index', compact('copa'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('modelo.copas.copas-create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::table('copas')->insert(
            array(
                "tipo" => $request->input('tipo'),
                "marca" => $request->input('marca'),
                "imagen" => $request->file('imagen')->store('imgs','public'),
            )
        );
        return redirect()->action([PrivateController::class, 'listarCartaBack']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $copa = Copa::where('id', '=', $id)->first();
        return view('modelo.copas.copas-show', compact('copa'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $copa = DB::table('copas')->where('id', '=', $id)->first();
        return view('modelo.copas.copas-edit', compact('copa'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $imagen=$request->file('imagen');
        if($imagen){
            DB::table('copas')->where('id', '=', $id)->update(
                array(
                    "tipo" => $request->input('tipo'),
                    "marca" => $request->input('marca'),
                    "imagen" => $request->file('imagen')->store('imgs','public'),
                )
            );
        }else{
            DB::table('copas')->where('id', '=', $id)->update(
                array(
                    "tipo" => $request->input('tipo'),
                    "marca" => $request->input('marca'),
                )
            );
        }
        
        return redirect()->action([PrivateController::class, 'listarRefrescosBack']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('copas')->where('id', '=', $id)->delete();
        return redirect()->back();
    }
}
