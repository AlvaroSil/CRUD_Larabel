<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Schema;

class Shishas extends Model
{
    use HasFactory;

    protected $table = 'shishas';


    protected $fillable = [
        'marca','modelo','imagen'
    ];

}
