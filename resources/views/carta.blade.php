<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href='{{asset("css/normalize.css")}}' rel='stylesheet' type='text/css'>
    <link href='{{asset("css/styles.css")}}' rel='stylesheet' type='text/css'>
    <script src='{{asset("js/scripts.js")}}'></script>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.js" integrity="sha256-DrT5NfxfbHvMHux31Lkhxg42LY6of8TaYyK50jnxRnM=" crossorigin="anonymous"></script>
    <script src='{{asset("js/buscador.js")}}'></script>
    <title>PsykoShishas</title>
</head>
<body>
<main>
@include ('includes.nav')

    <div class="cabecera">
        <img class="banner" src="img/carta_-01.png">
        <h1>Descubre nuestra carta</h1>
    </div>
    <center><input id="buscador" type="text"></center>
    <div class="lista">
        <ul>
            <li><a href="#refrescos">Refrescos</a></li>
            <li><a href="#batidos">Batidos</a></li>
            <li><a href="#cervezas">Cervezas</a></li>
            <li><a href="#copas">Copas</a></li>
            <li><a href="#cocteles">Cocteles</a></li>
            <li><a href="#comida">Comida</a></li>
            <li><a href="#melazas">Melazas</a></li>
        </ul>
    </div>
    <br>
    <div id="refrescos" class="titulo">
        <h2>Refrescos</h2>
        @foreach ($refresco as $r)
        <div class="producto">
            <img src='{{asset("storage/$r->imagen")}}'>             
            <a href="{{ route('refrescos-show', $r->id) }}"></a>
            <h2>{{ $r->nombre }}</h2>
        </div>
        @endforeach
    </div>
    <br>
    <div id="batidos" class="titulo">
        <h2>Batidos</h2>
        @foreach ($batido as $b)
        <div class="producto">
            <img src='{{asset("storage/$b->imagen")}}'>             
            <a href="{{ route('batidos-show', $b->id) }}"></a>
            <h2>{{ $b->nombre }}</h2>
        </div>
        @endforeach
    </div>
    <div id="cervezas" class="titulo">
        <h2>Cervezas</h2>
        @foreach ($cerveza as $b)
        <div class="producto">
            <img src='{{asset("storage/$b->imagen")}}'>             
            <a href="{{ route('cervezas-show', $b->id) }}"></a>
            <h2>{{ $b->marca }}</h2>
        </div>
        @endforeach
    </div>
    <div id="copas" class="titulo">
        <h2>Copas</h2>
        @foreach ($copa as $s)
        <div class="producto">
            <img src='{{asset("storage/$s->imagen")}}'>             
            <a href="{{ route('copas-show', $b->id) }}"></a>
            <h2>{{ $s->tipo }} {{ $s->marca }}</h2>
        </div>
        @endforeach
    </div>
    <div id="cocteles" class="titulo">
        <h2>Cocteles</h2>
        @foreach ($coctel as $b)
        <div class="producto">
            <img src='{{asset("storage/$b->imagen")}}'>             
            <a href="{{ route('cocteles-show', $b->id) }}"></a>
            <h2>{{ $b->nombre }}</h2>
        </div>
        @endforeach
    </div>
    <div id="comida" class="titulo">
        <h2>Comida</h2>
        @foreach ($comida as $b)
        <div class="producto">
            <img src='{{asset("storage/$b->imagen")}}'>             
            <a href="{{ route('comidas-show', $b->id) }}"></a>
            <h2>{{ $b->nombre }}</h2>
        </div>
        @endforeach
    </div>
    <div id="melazas" class="titulo">
        <h2>Melazas</h2>
        @foreach ($melaza as $b)
        <div class="producto">
            <img src='{{asset("storage/$b->imagen")}}'>             
            <a href="{{ route('melazas-show', $b->id) }}"></a>
            <h2>{{ $b->marca }}</h2>
        </div>
        @endforeach
    </div>


    @include ('includes.footer')

</main>

</body>
</html>