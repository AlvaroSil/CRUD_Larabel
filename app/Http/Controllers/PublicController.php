<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class PublicController extends Controller
{
    public function listarShishas()
    {        
        $shisha = DB::table('shishas')->get();
        return view('shishas', compact('shisha'));
    }
    public function listarRefrescos()
    {        
        $refresco = DB::table('refrescos')->get();
        $batido = DB::table('batidos')->get();
        $cerveza = DB::table('cervezas')->get();
        $copa = DB::table('copas')->get();
        $coctel = DB::table('cocteles')->get();
        $comida = DB::table('comidas')->get();
        $melaza = DB::table('melazas')->get();
        return view('carta', compact('batido','refresco','cerveza','copa','coctel','comida','melaza'));
    }
    
}
