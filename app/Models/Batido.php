<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Batidos extends Model
{
    use HasFactory;

    protected $table = 'batidos';


    protected $fillable = [
        'nombre','descripcion','imagen'
    ];
}
