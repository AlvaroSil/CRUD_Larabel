<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Melazas extends Model
{
    use HasFactory;

    protected $fillable = [
        'marca','sabor','imagen'
    ];
}
